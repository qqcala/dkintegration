FROM openjdk:8
EXPOSE 8080
ADD target/dkintegration.jar dkintegration.jar
ENTRYPOINT ["java","-jar","/dkintegration.jar"]